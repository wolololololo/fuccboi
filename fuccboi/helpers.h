#pragma once

#include "defs.h"

void Sleep(long long ms);
/// Disables interrupts and sets with Write Protection bit(16) to 0
/// This shall have a matching SetCr0
/// @return: The original value of the cr0 register, give this value back to SetCr0
uintptr_t DisableWP(void);
/// Sets the cr0 register and enables interrupts
/// This is the counterpart of DisableWP
/// @param(newValue): The new value to set cr0 which sould be the result of DisableWP
void SetCr0(uintptr_t newValue);
