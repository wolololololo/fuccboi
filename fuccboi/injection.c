
#include "pe_structs.h"
#include "injection.h"
#include "loader.h"
#include "helpers.h"
#include "file.h"
#include "thread.h"
#include "pattern.h"
#include "debug.h"
#include "imports.h"

typedef void *(_stdcall *LoadLibraryAFn)(uintptr_t filename);
typedef uintptr_t(_stdcall *GetProcAddressFn)(void *module, uintptr_t procName);
//typedef int (_stdcall *DllMainFn)(uintptr_t hinstDLL, unsigned long fdwReason, void *lpvReserved); 
typedef NTSTATUS(_stdcall *LdrpHandleTlsDataFn)(PLDR_DATA_TABLE_ENTRY ModuleEntry);

typedef struct
{
    uintptr_t ImageBase;
    IMAGE_NT_HEADERS64 *NtHeaders;
    IMAGE_BASE_RELOCATION *BaseReloc;
    IMAGE_IMPORT_DESCRIPTOR *ImportDesc;
    LoadLibraryAFn LoadLibraryA;
    GetProcAddressFn GetProcAddress;
    LdrpHandleTlsDataFn LdrpHandleTlsData;
} ManualInjectionParams;

static const unsigned char ManualMapCodeStub[] =
{
    /*
int __fastcall LoadDll(ManualInjectionParams *params)
{
    uintptr_t delta = params->ImageBase - params->NtHeaders->OptionalHeader.ImageBase;
    // Relocate the image
    for (IMAGE_BASE_RELOCATION *reloc = params->BaseReloc;
        reloc && reloc->VirtualAddress;
        reloc = (IMAGE_BASE_RELOCATION*)((uintptr_t)reloc + reloc->SizeOfBlock))
    {
        if (reloc->SizeOfBlock >= sizeof(*reloc))
        {
            uintptr_t count = (reloc->SizeOfBlock - sizeof(*reloc)) / sizeof(WORD);
            WORD *relocs = (WORD*)(reloc + 1);
            for (uintptr_t i = 0; i < count; ++i)
            {
                if (relocs[i])
                {
                    uintptr_t *r = (uintptr_t*)(params->ImageBase + (reloc->VirtualAddress + (relocs[i] & 0x0FFF)));
                    *r += delta;
                }
            }
        }
    }

    for (IMAGE_IMPORT_DESCRIPTOR *import = params->ImportDesc;
        import && import->Characteristics;
        ++import)
    {
        IMAGE_THUNK_DATA *originalFirstThunk = (IMAGE_THUNK_DATA*)(params->ImageBase + import->OriginalFirstThunk);
        IMAGE_THUNK_DATA *firstThunk = (IMAGE_THUNK_DATA*)(params->ImageBase + import->FirstThunk);
        void *module = params->LoadLibraryA(params->ImageBase + import->Name);
        if (!module)
        {
            return 0;
        }

        while (originalFirstThunk->u1.AddressOfData)
        {
            if (originalFirstThunk->u1.Ordinal & IMAGE_ORDINAL_FLAG64)
            {
                // Import by ordinal
                uintptr_t function = params->GetProcAddress(module, originalFirstThunk->u1.Ordinal & 0xFFFF);
                if (!function)
                {
                    return 0;
                }
                firstThunk->u1.Function = function;
            }
            else
            {
                // Import by name
                IMAGE_IMPORT_BY_NAME *name = (IMAGE_IMPORT_BY_NAME*)(params->ImageBase + originalFirstThunk->u1.AddressOfData);
                uintptr_t function = params->GetProcAddress(module, (uintptr_t)name->Name);
                if (!function)
                {
                    return 0;
                }
                firstThunk->u1.Function = function;
            }
            ++originalFirstThunk;
            ++firstThunk;
        }
    }
    if (params->LdrpHandleTlsData)
    {
        LDR_DATA_TABLE_ENTRY entry = { 0 };
        entry.DllBase = (void*)params->ImageBase;
        params->LdrpHandleTlsData(&entry);
    }
    if (params->NtHeaders->OptionalHeader.AddressOfEntryPoint)
    {
        DllMainFn DllMain = (DllMainFn)(params->ImageBase + params->NtHeaders->OptionalHeader.AddressOfEntryPoint);
        return DllMain(params->ImageBase, DLL_PROCESS_ATTACH, 0);
    }
    return 1;
}
    */
    0x40, 0x56, 0x41, 0x56, 0x48, 0x81, 0xEC, 0xB8, 0x00, 0x00, 0x00, 0x48, 0x8B, 0x41, 0x08, 0x48, 0x8B, 0xF1, 0x4C, 0x8B, 0x09, 0x4C, 0x8B, 0x41, 0x10, 0x4C, 0x2B, 0x48, 0x30, 0x4D, 0x85, 0xC0, 0x74, 0x47, 0x41, 0x83, 0x38, 0x00, 0x74, 0x41, 0x41, 0x8B, 0x40, 0x04, 0x83, 0xF8, 0x08, 0x72, 0x2F, 0x48, 0x8D, 0x50, 0xF8, 0x48, 0xD1, 0xEA, 0x74, 0x26, 0x49, 0x8D, 0x48, 0x08, 0x66, 0x90, 0x0F, 0xB7, 0x01, 0x66, 0x85, 0xC0, 0x74, 0x0E, 0x25, 0xFF, 0x0F, 0x00, 0x00, 0x41, 0x03, 0x00, 0x48, 0x03, 0x06, 0x4C, 0x01, 0x08, 0x48, 0x83, 0xC1, 0x02, 0x48, 0x83, 0xEA, 0x01, 0x75, 0xE0, 0x41, 0x8B, 0x40, 0x04, 0x4C, 0x03, 0xC0, 0x75, 0xB9, 0x4C, 0x8B, 0x76, 0x18, 0x48, 0x89, 0x9C, 0x24, 0xD8, 0x00, 0x00, 0x00, 0x48, 0x89, 0xAC, 0x24, 0xE0, 0x00, 0x00, 0x00, 0x48, 0x89, 0xBC, 0x24, 0xB0, 0x00, 0x00, 0x00, 0x4D, 0x85, 0xF6, 0x74, 0x75, 0x66, 0x0F, 0x1F, 0x44, 0x00, 0x00, 0x41, 0x8B, 0x0E, 0x85, 0xC9, 0x74, 0x68, 0x48, 0x8B, 0x06, 0x41, 0x8B, 0x7E, 0x10, 0x48, 0x03, 0xF8, 0x48, 0x8D, 0x1C, 0x08, 0x41, 0x8B, 0x4E, 0x0C, 0x48, 0x03, 0xC8, 0xFF, 0x56, 0x20, 0x48, 0x8B, 0xE8, 0x48, 0x85, 0xC0, 0x0F, 0x84, 0xBD, 0x00, 0x00, 0x00, 0x48, 0x8B, 0x0B, 0x48, 0x85, 0xC9, 0x74, 0x36, 0x48, 0x2B, 0xFB, 0x48, 0x85, 0xC9, 0x79, 0x05, 0x0F, 0xB7, 0xD1, 0xEB, 0x0A, 0x48, 0x8B, 0x16, 0x48, 0x83, 0xC2, 0x02, 0x48, 0x03, 0xD1, 0x48, 0x8B, 0xCD, 0xFF, 0x56, 0x28, 0x48, 0x85, 0xC0, 0x0F, 0x84, 0x8F, 0x00, 0x00, 0x00, 0x48, 0x89, 0x04, 0x1F, 0x48, 0x83, 0xC3, 0x08, 0x48, 0x8B, 0x0B, 0x48, 0x85, 0xC9, 0x75, 0xD0, 0x49, 0x83, 0xC6, 0x14, 0x75, 0x91, 0x48, 0x8B, 0x56, 0x30, 0x48, 0x85, 0xD2, 0x74, 0x53, 0x45, 0x33, 0xC0, 0x48, 0x8D, 0x44, 0x24, 0x20, 0x41, 0x8D, 0x48, 0x02, 0x0F, 0x1F, 0x40, 0x00, 0x0F, 0x1F, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4C, 0x89, 0x00, 0x4C, 0x89, 0x40, 0x08, 0x4C, 0x89, 0x40, 0x10, 0x48, 0x8D, 0x40, 0x40, 0x4C, 0x89, 0x40, 0xD8, 0x4C, 0x89, 0x40, 0xE0, 0x4C, 0x89, 0x40, 0xE8, 0x4C, 0x89, 0x40, 0xF0, 0x4C, 0x89, 0x40, 0xF8, 0x48, 0x83, 0xE9, 0x01, 0x75, 0xD7, 0x4C, 0x89, 0x00, 0x48, 0x8D, 0x4C, 0x24, 0x20, 0x48, 0x8B, 0x06, 0x48, 0x89, 0x44, 0x24, 0x50, 0xFF, 0xD2, 0x48, 0x8B, 0x46, 0x08, 0x8B, 0x50, 0x28, 0x85, 0xD2, 0x74, 0x16, 0x48, 0x8B, 0x0E, 0x45, 0x33, 0xC0, 0x48, 0x8D, 0x04, 0x11, 0x41, 0x8D, 0x50, 0x01, 0xFF, 0xD0, 0xEB, 0x09, 0x33, 0xC0, 0xEB, 0x05, 0xB8, 0x01, 0x00, 0x00, 0x00, 0x48, 0x8B, 0xBC, 0x24, 0xB0, 0x00, 0x00, 0x00, 0x48, 0x8B, 0xAC, 0x24, 0xE0, 0x00, 0x00, 0x00, 0x48, 0x8B, 0x9C, 0x24, 0xD8, 0x00, 0x00, 0x00, 0x48, 0x81, 0xC4, 0xB8, 0x00, 0x00, 0x00, 0x41, 0x5E, 0x5E, 0xC3
};

static NTSTATUS ManualMapImageInUserThread(HANDLE pid, PEPROCESS process, byte *image, HANDLE *out_ourModule)
{
    if (!process)
    {
        DPRINT("process null");
        return STATUS_INVALID_PARAMETER;
    }
    if (!image)
    {
        DPRINT("image null");
        return STATUS_INVALID_PARAMETER;
    }
    if (!out_ourModule)
    {
        DPRINT("out_ourModule null");
        return STATUS_INVALID_PARAMETER;
    }

    NTSTATUS status = STATUS_SUCCESS;
    byte *dllImage = NULL;
    size_t dllImageSize = 0;
    ThreadStartInfo startInfo = { 0 };
    byte *manualMapBuffer = NULL;
    size_t manualMapBufferSize = sizeof(ManualInjectionParams);
    manualMapBufferSize = (manualMapBufferSize + 0x10) & ~0x0F; // 16 byte align this
    manualMapBufferSize += sizeof(ManualMapCodeStub);
    void *kernel32 = FindUserModuleBase(process, L"kernel32.dll");
    if (!kernel32)
    {
        DPRINT("Didn't find kernel32.dll!");
        status = -1;
        goto cleanup;
    }
    else
    {
        DPRINT("kernel32.dll: [%p]", kernel32);
    }
    LoadLibraryAFn LoadLibraryA = (LoadLibraryAFn)(uintptr_t)FindUserModuleExport(process, kernel32, "LoadLibraryA");
    if (!LoadLibraryA)
    {
        DPRINT("Didn't find LoadLibraryA!");
        status = -1;
        goto cleanup;
    }
    else
    {
        DPRINT("LoadLibraryA: [%p]", LoadLibraryA);
    }
    GetProcAddressFn GetProcAddress = (GetProcAddressFn)(uintptr_t)FindUserModuleExport(process, kernel32, "GetProcAddress");
    if (!GetProcAddress)
    {
        DPRINT("Didn't find GetProcAddress!");
        status = -1;
        goto cleanup;
    }
    else
    {
        DPRINT("GetProcAddress: [%p]", GetProcAddress);
    }
    void *ntdll = FindUserModuleBase(process, L"ntdll.dll");
    if (!ntdll)
    {
        DPRINT("Didn't find ntdll.dll!");
        status = -1;
        goto cleanup;
    }

    const char *pattern = "\x48\x89\x5C\x24\x00\x48\x89\x74\x24\x00\x57\x41\x54\x41\x55\x41\x56\x41\x57\x48\x81\xEC\x00\x00\x00\x00\x48\x8B\x05\x00\x00\x00\x00\x48\x33\xC4\x48\x89\x84\x24\x00\x00\x00\x00\x48\x89\x4C\x24\x00";
    const char *mask = "xxxx?xxxx?xxxxxxxxxxxx????xxx????xxxxxxx????xxxx?";
    LdrpHandleTlsDataFn LdrpHandleTlsData = (LdrpHandleTlsDataFn)(uintptr_t)FindPattern(ntdll, 0x100000, pattern, mask);
    if (!LdrpHandleTlsData)
    {
        DPRINT("Didn't find LdrpHandleTlsData, TLS probably won't work!");
    }
    else
    {
        DPRINT("LdrpHandleTlsData: [%p]", LdrpHandleTlsData);
    }

    status = ZwAllocateVirtualMemory(ZwCurrentProcess(), &manualMapBuffer, 0, &manualMapBufferSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
    if (!NT_SUCCESS(status))
    {
        DPRINT("Failed to allocate RWX buffer for manual map shellcode : NTSTATUS(%#x)", status);
        goto cleanup;
    }
    memset(manualMapBuffer, 0xCC, manualMapBufferSize);
    ManualInjectionParams *params = (ManualInjectionParams*)manualMapBuffer;
    RtlSecureZeroMemory(params, sizeof(*params));
    byte *manualMapShellcode = manualMapBuffer + sizeof(ManualInjectionParams);
    manualMapShellcode = (byte*)(((uintptr_t)manualMapShellcode + 0x10) & ~0x0F); // 16 byte align
    memcpy(manualMapShellcode, ManualMapCodeStub, sizeof(ManualMapCodeStub));
    IMAGE_DOS_HEADER *dosHeader = (IMAGE_DOS_HEADER*)image;
    IMAGE_NT_HEADERS64 *ntHeader = (IMAGE_NT_HEADERS64*)(image + dosHeader->e_lfanew);
    dllImageSize = ntHeader->OptionalHeader.SizeOfImage;
    status = ZwAllocateVirtualMemory(ZwCurrentProcess(), &dllImage, 0, &dllImageSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
    if (!NT_SUCCESS(status))
    {
        DPRINT("Failed to allocate RWX buffer for manual map shellcode : NTSTATUS(%#x)", status);
        goto cleanup;
    }
    // Copy headers
    memcpy(dllImage, image, ntHeader->OptionalHeader.SizeOfHeaders);
    // Copy sections
    IMAGE_SECTION_HEADER *sections = (IMAGE_SECTION_HEADER*)(ntHeader + 1);
    for (size_t i = 0; i < ntHeader->FileHeader.NumberOfSections; ++i)
    {
        void *dst = dllImage + sections[i].VirtualAddress;
        void *src = image + sections[i].PointerToRawData;
        size_t size = sections[i].SizeOfRawData;
        memcpy(dst, src, size);
    }

    params->ImageBase = (uintptr_t)dllImage;
    params->LoadLibraryA = LoadLibraryA;
    params->GetProcAddress = GetProcAddress;
    params->LdrpHandleTlsData = LdrpHandleTlsData;
    params->NtHeaders = (IMAGE_NT_HEADERS64*)(params->ImageBase + dosHeader->e_lfanew);
    params->BaseReloc = (IMAGE_BASE_RELOCATION*)(params->ImageBase + ntHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress);
    params->ImportDesc = (IMAGE_IMPORT_DESCRIPTOR*)(params->ImageBase + ntHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);

    startInfo.Timeout.QuadPart = -(60LL * 1000 * 10000); // 60s timeout
    //status = ExecuteInUserThread(&startInfo, manualMapShellcode, params);
    status = HijackThreadWithApc(pid, manualMapShellcode, params);
    if (!NT_SUCCESS(status))
    {
        DPRINT("ExecuteInUserThread failed with NTSTATUS(%#x)", status);
        goto cleanup;
    }
    DPRINT("User TID:%#x", startInfo.ThreadId);
    DPRINT("User Thread exited with: %#x", startInfo.ThreadExitCode);
    DPRINT("Our module base [%p]", dllImage);
    // Optional: Clear the PE headers
    RtlSecureZeroMemory(dllImage, ntHeader->OptionalHeader.SizeOfHeaders);
    *out_ourModule = dllImage;

cleanup:
    if (manualMapBuffer)
    {
        RtlSecureZeroMemory(manualMapBuffer, manualMapBufferSize);
        ZwFreeVirtualMemory(ZwCurrentProcess(), &manualMapBuffer, &manualMapBufferSize, MEM_RELEASE);
    }
    if (!NT_SUCCESS(status) && dllImage) 
    {
        RtlSecureZeroMemory(dllImage, dllImageSize);
        ZwFreeVirtualMemory(ZwCurrentProcess(), &dllImage, &dllImageSize, MEM_RELEASE);
    }
    return status;
}

NTSTATUS ExecManualMapInUserThread(HANDLE pid, PEPROCESS process, UNICODE_STRING *moduleFilename, HANDLE *out_ourModule)
{
    if (!process)
    {
        DPRINT("process null");
        return STATUS_INVALID_PARAMETER;
    }
    if (!moduleFilename)
    {
        DPRINT("moduleFilename null");
        return STATUS_INVALID_PARAMETER;
    }
    if (!out_ourModule)
    {
        DPRINT("out_ourModule null");
        return STATUS_INVALID_PARAMETER;
    }

    byte *fileContents = NULL;
    size_t fileLength = 0;
    NTSTATUS status = ReadFile(moduleFilename, &fileContents, &fileLength);
    if (!NT_SUCCESS(status))
    {
        return status;
    }
    status = ManualMapImageInUserThread(pid, process, fileContents, out_ourModule);
    if (fileContents)
    {
        ExFreePool(fileContents);
    }
    return status;
}


typedef void *(NTAPI *LoadLibraryWFn)(IN LPCWSTR lpFileName);
NTSTATUS ExecLoadLibraryWInUserThread(PEPROCESS process, UNICODE_STRING *moduleFilename, HANDLE *ourModule)
{
    const byte code[] =
    {
        0x48, 0x83, 0xEC, 0x28,                 // sub rsp, 0x28
        0x48, 0xB9, 0,0,0,0, 0,0,0,0,           // mov rcx, FilePath      offset +6
        0x48, 0xB8, 0,0,0,0, 0,0,0,0,           // mov rax, LoadLibraryW  offset +16
        0xFF, 0xD0,                             // call rax
        0x48, 0xBA, 0,0,0,0, 0,0,0,0,           // mov rdx, OurModule     offset +28
        0x48, 0x89, 0x02,                       // mov [rdx], rax
        0x48, 0x83, 0xC4, 0x28,                 // add rsp, 0x28
        0x48, 0x31, 0xC0,                       // xor rax, rax
        0xC3                                    // ret
    };

    ThreadStartInfo startInfo = { 0 };
    byte *buffer = NULL;
    SIZE_T bufferSize = 0x1000;
    NTSTATUS status = ZwAllocateVirtualMemory(ZwCurrentProcess(), &buffer, 0, &bufferSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
    if (!NT_SUCCESS(status))
    {
        DPRINT("Failed to allocate RWX buffer for thread shellcode : NTSTATUS(%#x)", status);
        goto cleanup;
    }
    RtlSecureZeroMemory(buffer, bufferSize);

    void *kernel32 = FindUserModuleBase(process, L"kernel32.dll");
    if (!kernel32)
    {
        DPRINT("Didn't find kernel32.dll!");
        goto cleanup;
    }
    else
    {
        DPRINT("kernel32.dll: [%p]", kernel32);
    }

    LoadLibraryWFn LoadLibraryW = (LoadLibraryWFn)(uintptr_t)FindUserModuleExport(process, kernel32, "LoadLibraryW");
    if (!LoadLibraryW)
    {
        DPRINT("Didn't find LoadLibraryW!");
        goto cleanup;
    }
    else
    {
        DPRINT("LoadLibraryW: [%p]", LoadLibraryW);
    }

#pragma warning(disable : 4200)
    struct UserBuffer
    {
        wchar_t ModuleFilenameBuffer[261/*MAX_PATH+1*/];
        HANDLE OurModule;
        byte Shellcode[0];
    };
#pragma warning(default : 4200)
    struct UserBuffer *userBuffer = (struct UserBuffer*)buffer;
    if (moduleFilename->Length > sizeof(userBuffer->ModuleFilenameBuffer))
    {
        DPRINT("DllPath too long...");
        status = -1;
        goto cleanup;
    }
    // We want everything to be within this user buffer so we must make the module filename point to memory in this buffer too
    memcpy(userBuffer->ModuleFilenameBuffer, moduleFilename->Buffer, moduleFilename->Length);

    memcpy(userBuffer->Shellcode, code, sizeof(code));
    *(uintptr_t*)(&userBuffer->Shellcode[6]) = (uintptr_t)&userBuffer->ModuleFilenameBuffer;
    *(uintptr_t*)(&userBuffer->Shellcode[16]) = (uintptr_t)LoadLibraryW;
    *(uintptr_t*)(&userBuffer->Shellcode[28]) = (uintptr_t)&userBuffer->OurModule;

    startInfo.Timeout.QuadPart = -(5000 * 10000ll); // 5s timeout
    status = ExecuteInUserThread(&startInfo, userBuffer->Shellcode, NULL);
    if (!NT_SUCCESS(status))
    {
        DPRINT("ExecuteInUserThread failed with NTSTATUS(%#x)", status);
        goto cleanup;
    }
    DPRINT("User TID:%#x", startInfo.ThreadId);
    DPRINT("User Thread exited with: %#x", startInfo.ThreadExitCode);
    DPRINT("Our module base [%p]", userBuffer->OurModule);
    if (ourModule)
    {
        *ourModule = userBuffer->OurModule;
    }
cleanup:
    if (buffer)
    {
        RtlSecureZeroMemory(buffer, bufferSize);
        ZwFreeVirtualMemory(ZwCurrentProcess(), &buffer, &bufferSize, MEM_RELEASE);
    }
    return status;
}

typedef NTSTATUS(NTAPI *LdrLoadDllFn)(PWCHAR PathToFile, ULONG Flags, PUNICODE_STRING ModuleFileName, PHANDLE ModuleHandle);
NTSTATUS ExecLdrLoadDllInUserThread(PEPROCESS process, UNICODE_STRING *moduleFilename, HANDLE *ourModule, NTSTATUS *ldrLoadDllStatus)
{
    const byte code[] = 
    {
        0x48, 0x83, 0xEC, 0x28,                 // sub rsp, 0x28
        0x48, 0x31, 0xC9,                       // xor rcx, rcx
        0x48, 0x31, 0xD2,                       // xor rdx, rdx
        0x49, 0xB8, 0,0,0,0, 0,0,0,0,           // mov r8, ModuleFileName   offset +12
        0x49, 0xB9, 0,0,0,0, 0,0,0,0,           // mov r9, ModuleHandle     offset +22
        0x48, 0xB8, 0,0,0,0, 0,0,0,0,           // mov rax, LdrLoadDll      offset +32
        0xFF, 0xD0,                             // call rax
        0x48, 0xBA, 0,0,0,0, 0,0,0,0,           // mov rdx, LdrLoadDllStatus offset +44
        0x89, 0x02,                             // mov [rdx], eax
        0x48, 0x83, 0xC4, 0x28,                 // add rsp, 0x28
        0xC3                                    // ret  ; NTSTATUS from LdrLoadDll already in rax
    };

    ThreadStartInfo startInfo = { 0 };
    byte *buffer = NULL;
    SIZE_T bufferSize = 0x1000;
    NTSTATUS status = ZwAllocateVirtualMemory(ZwCurrentProcess(), &buffer, 0, &bufferSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
    if (!NT_SUCCESS(status))
    {
        DPRINT("Failed to allocate RWX buffer for thread shellcode : NTSTATUS(%#x)", status);
        goto cleanup;
    }
    RtlSecureZeroMemory(buffer, bufferSize);

    void *ntdll = FindUserModuleBase(process, L"ntdll.dll");
    if (!ntdll)
    {
        DPRINT("Didn't find ntdll.dll!");
        goto cleanup;
    }
    else
    {
        DPRINT("ntdll.dll: [%p]", ntdll);
    }
    LdrLoadDllFn LdrLoadDll = (LdrLoadDllFn)(uintptr_t)FindUserModuleExport(process, ntdll, "LdrLoadDll");
    if (!LdrLoadDll)
    {
        DPRINT("Didn't find LdrLoadDll!");
        goto cleanup;
    }
    else
    {
        DPRINT("LdrLoadDll: [%p]", LdrLoadDll);
    }

#pragma warning(disable : 4200)
    struct UserBuffer
    {
        UNICODE_STRING ModuleFilename;
        wchar_t ModuleFilenameBuffer[261/*MAX_PATH+1*/];
        HANDLE OurModule;
        NTSTATUS LdrLoadDllStatus;
        byte Shellcode[0];
    };
#pragma warning(default : 4200)
    struct UserBuffer *userBuffer = (struct UserBuffer*)buffer;
    if (moduleFilename->Length > sizeof(userBuffer->ModuleFilenameBuffer))
    {
        DPRINT("DllPath too long...");
        status = -1;
        goto cleanup;
    }
    // We want everything to be within this user buffer so we must make the module filename point to memory in this buffer too
    // Copy path into user buffer
    userBuffer->ModuleFilename.Length = min(moduleFilename->Length, sizeof(userBuffer->ModuleFilenameBuffer));
    userBuffer->ModuleFilename.MaximumLength = min(moduleFilename->MaximumLength, sizeof(userBuffer->ModuleFilenameBuffer));
    userBuffer->ModuleFilename.Buffer = userBuffer->ModuleFilenameBuffer;
    memcpy(userBuffer->ModuleFilename.Buffer, moduleFilename->Buffer, userBuffer->ModuleFilename.Length);
    // Copy shellcode into user buffer
    memcpy(userBuffer->Shellcode, code, sizeof(code));
    // Fix shellcode addresses
    *(uintptr_t*)(&userBuffer->Shellcode[12]) = (uintptr_t)&userBuffer->ModuleFilename;
    *(uintptr_t*)(&userBuffer->Shellcode[22]) = (uintptr_t)&userBuffer->OurModule;
    *(uintptr_t*)(&userBuffer->Shellcode[32]) = (uintptr_t)LdrLoadDll;
    *(uintptr_t*)(&userBuffer->Shellcode[44]) = (uintptr_t)&userBuffer->LdrLoadDllStatus;
    // Execute our shellcode in a User thread

    startInfo.Timeout.QuadPart = -(5000 * 10000ll); // 5s timeout
    status = ExecuteInUserThread(&startInfo, userBuffer->Shellcode, NULL);
    if (!NT_SUCCESS(status))
    {
        DPRINT("ExecuteInUserThread failed with NTSTATUS(%#x)", status);
        goto cleanup;
    }
    DPRINT("User TID:%#x", startInfo.ThreadId);
    DPRINT("User Thread exited with: %#x", startInfo.ThreadExitCode);
    DPRINT("Our module base [%p]", userBuffer->OurModule);
    if (ourModule)
    {
        *ourModule = userBuffer->OurModule;
    }
    DPRINT("LdrLoadDll return status [%p]", userBuffer->LdrLoadDllStatus);
    if (ldrLoadDllStatus)
    {
        *ldrLoadDllStatus = userBuffer->LdrLoadDllStatus;
    }
cleanup:
    if (buffer)
    {
        RtlSecureZeroMemory(buffer, bufferSize);
        ZwFreeVirtualMemory(ZwCurrentProcess(), &buffer, &bufferSize, MEM_RELEASE);
    }
    return status;
}

static void EraseHeaders(void *ourModule)
{
    if (!ourModule)
    {
        return;
    }
    // Scrub the DOS and NT headers
    IMAGE_DOS_HEADER *dosHeader = (IMAGE_DOS_HEADER*)ourModule;
    if (dosHeader->e_magic == IMAGE_DOS_SIGNATURE)
    {
        DPRINT("DOS signature found!");
        IMAGE_NT_HEADERS64 *ntHeader = (IMAGE_NT_HEADERS64*)((uintptr_t)ourModule + dosHeader->e_lfanew);
        if (ntHeader->Signature == IMAGE_NT_SIGNATURE)
        {
            DPRINT("PE signature found!");
            // These headers are in read-only memory.
            uintptr_t oldCr0 = DisableWP();
            RtlSecureZeroMemory(&ntHeader->OptionalHeader, ntHeader->FileHeader.SizeOfOptionalHeader);
            RtlSecureZeroMemory(&ntHeader->FileHeader, sizeof(ntHeader->FileHeader));
            RtlSecureZeroMemory(ntHeader, sizeof(*ntHeader));
            RtlSecureZeroMemory(dosHeader, sizeof(*dosHeader));
            SetCr0(oldCr0);
        }
        else
        {
            DPRINT("PE header signature not correct??");
        }
    }
    else
    {
        DPRINT("MZ not at start of our module??");
    }
}

static void InjectDllIntoPid_LoadLibraryWInUserThread(wchar_t *dllPath, HANDLE pid)
{
    PEPROCESS process = NULL;
    NTSTATUS status = PsLookupProcessByProcessId(pid, &process);
    if (!NT_SUCCESS(status))
    {
        DPRINT("Failed to lookup pid %#x : NTSTATUS(%#x)", pid, status);
        goto cleanup;
    }
    LARGE_INTEGER timeout = { 0 };
    if (KeWaitForSingleObject(process, Executive, KernelMode, FALSE, &timeout) == STATUS_WAIT_0)
    {
        DPRINT("Process is exiting...");
        goto cleanup;
    }

    KAPC_STATE apcState;
    KeStackAttachProcess(process, &apcState);

    UNICODE_STRING moduleFileName = { 0 };
    RtlInitUnicodeString(&moduleFileName, dllPath);
    HANDLE ourModule = NULL;
    status = ExecLoadLibraryWInUserThread(process, &moduleFileName, &ourModule);
    DPRINT("Our module: [%p]", ourModule);
    if (!NT_SUCCESS(status))
    {
        DPRINT("Failed to load our dll! NTSTATUS(%#x) (%wZ)", status, &moduleFileName);
        goto detach;
    }

    if (UnlinkUserModuleFromPeb(process, ourModule))
    {
        DPRINT("Unlinked module [%p] from PEB!", ourModule);
        EraseHeaders(ourModule);
    }
    else
    {
        DPRINT("Failed to unlink module [%p] from PEB!", ourModule);
    }

detach:
    KeUnstackDetachProcess(&apcState);
cleanup:
    if (process) 
    {
        ObDereferenceObject(process);
    }
}

static void InjectDllIntoPid_LdrLoadDllInUserThread(wchar_t *dllPath, HANDLE pid)
{
    PEPROCESS process = NULL;
    NTSTATUS status = PsLookupProcessByProcessId(pid, &process);
    if (!NT_SUCCESS(status))
    {
        DPRINT("Failed to lookup pid %#x : NTSTATUS(%#x)", pid, status);
        goto cleanup;
    }
    LARGE_INTEGER timeout = { 0 };
    if (KeWaitForSingleObject(process, Executive, KernelMode, FALSE, &timeout) == STATUS_WAIT_0)
    {
        DPRINT("Process is exiting...");
        goto cleanup;
    }

    KAPC_STATE apcState;
    KeStackAttachProcess(process, &apcState);

    UNICODE_STRING moduleFileName = { 0 };
    RtlInitUnicodeString(&moduleFileName, dllPath);
    HANDLE ourModule = NULL;
    DPRINT("Attempting to run LdrLoadDll in user thread!");
    NTSTATUS ldrLoadDllStatus = 0;
    status = ExecLdrLoadDllInUserThread(process, &moduleFileName, &ourModule, &ldrLoadDllStatus);
    DPRINT("Our module: [%p]", ourModule);
    if (!NT_SUCCESS(status))
    {
        DPRINT("Failed to load our dll! NTSTATUS(%#x) (%wZ)", status, &moduleFileName);
        goto detach;
    }

    if (UnlinkUserModuleFromPeb(process, ourModule))
    {
        DPRINT("Unlinked module [%p] from PEB!", ourModule);
        EraseHeaders(ourModule);
    }
    else
    {
        DPRINT("Failed to unlink module [%p] from PEB!", ourModule);
    }

detach:
    KeUnstackDetachProcess(&apcState);
cleanup:
    if (process) 
    {
        ObDereferenceObject(process);
    }
}

NTSTATUS ManualMapImageIntoPid(byte *image, HANDLE pid)
{
    PEPROCESS process = NULL;
    NTSTATUS status = PsLookupProcessByProcessId(pid, &process);
    if (!NT_SUCCESS(status))
    {
        DPRINT("Failed to lookup pid %#x : NTSTATUS(%#x)", pid, status);
        goto cleanup;
    }
    DPRINT("process: [%p]", process);
    if (!process)
    {
        status = STATUS_UNSUCCESSFUL;
        goto cleanup;
    }
    //LARGE_INTEGER timeout = { 0 };
    //if (KeWaitForSingleObject(process, Executive, KernelMode, FALSE, &timeout) == STATUS_WAIT_0)
    //{
    //    DPRINT("Process is exiting...");
    //    goto cleanup;
    //}

    KAPC_STATE apcState;
    KeStackAttachProcess(process, &apcState);

    HANDLE ourModule = NULL;
    status = ManualMapImageInUserThread(pid, process, image, &ourModule);
    if (!NT_SUCCESS(status))
    {
        DPRINT("ManualMapImageInUserThread returned NTSTATUS(%#x)", status);
        goto detach;
    }
    DPRINT("Our module: [%p]", ourModule);
    if (!NT_SUCCESS(status))
    {
        DPRINT("Failed to load our dll! NTSTATUS(%#x)", status);
        goto detach;
    }
    status = STATUS_SUCCESS;
detach:
    KeUnstackDetachProcess(&apcState);
cleanup:
    if (process) 
    {
        ObDereferenceObject(process);
    }
    return status;
}

static void InjectDllIntoPid_ManualMapInUserThread(wchar_t *dllPath, HANDLE pid)
{
    PEPROCESS process = NULL;
    NTSTATUS status = PsLookupProcessByProcessId(pid, &process);
    if (!NT_SUCCESS(status))
    {
        DPRINT("Failed to lookup pid %#x : NTSTATUS(%#x)", pid, status);
        goto cleanup;
    }
    LARGE_INTEGER timeout = { 0 };
    if (KeWaitForSingleObject(process, Executive, KernelMode, FALSE, &timeout) == STATUS_WAIT_0)
    {
        DPRINT("Process is exiting...");
        goto cleanup;
    }

    KAPC_STATE apcState;
    KeStackAttachProcess(process, &apcState);

    UNICODE_STRING moduleFileName = { 0 };
    RtlInitUnicodeString(&moduleFileName, dllPath);
    HANDLE ourModule = NULL;
    status = ExecManualMapInUserThread(pid, process, &moduleFileName, &ourModule);
    if (!NT_SUCCESS(status))
    {
        DPRINT("ExecManualMapInUserThread returned NTSTATUS(%#x)", status);
        goto detach;
    }
    DPRINT("Our module: [%p]", ourModule);
    if (!NT_SUCCESS(status))
    {
        DPRINT("Failed to load our dll! NTSTATUS(%#x) (%wZ)", status, &moduleFileName);
        goto detach;
    }

detach:
    KeUnstackDetachProcess(&apcState);
cleanup:
    if (process) 
    {
        ObDereferenceObject(process);
    }
}


void InjectDllIntoPid(InjectionMethod method, wchar_t *dllPath, HANDLE pid)
{
    if (!dllPath)
    {
        DPRINT("dllPath null");
        return;
    }
    switch (method)
    {
    case Inject_ManualMapInUserThread:
        InjectDllIntoPid_ManualMapInUserThread(dllPath, pid);
        break;
    case Inject_LoadLibraryWInUserThread:
        InjectDllIntoPid_LoadLibraryWInUserThread(dllPath, pid);
        break;
    case Inject_LdrLoadDllInUserthread:
        InjectDllIntoPid_LdrLoadDllInUserThread(dllPath, pid);
        break;
    }
}

