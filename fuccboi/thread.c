#include "thread.h"
#include "helpers.h"
#include "debug.h"
#include "imports.h"

NTSTATUS ExecuteInUserThread(ThreadStartInfo *threadInfo, void *codeAddress, void *params)
{
    if (!threadInfo)
    {
        DPRINT("threadInfo null");
        return STATUS_INVALID_PARAMETER;
    }
    if (!codeAddress)
    {
        DPRINT("codeAddress null");
        return STATUS_INVALID_PARAMETER;
    }

    // Execute our shellcode in a User thread
    HANDLE thread = NULL;
    CLIENT_ID cid = { 0 };
    NTSTATUS status = RtlCreateUserThread(
        ZwCurrentProcess(),
        NULL, FALSE, 0, NULL, NULL,
        codeAddress, params,
        &thread, &cid);

    if (!NT_SUCCESS(status))
    {
        DPRINT("Failed to create user thread : NTSTATUS(%#x)", status);
        goto cleanup;
    }

    if (threadInfo->Timeout.QuadPart != 0) 
    {
        status = ZwWaitForSingleObject(thread, TRUE, &threadInfo->Timeout);
        if (!NT_SUCCESS(status))
        {
            DPRINT("Thread timed out... : NTSTATUS(%#x)", status);
            goto cleanup;
        }
    }

    THREAD_BASIC_INFORMATION tInfo = { 0 };
    ULONG bytes = 0;
    status = ZwQueryInformationThread(thread, ThreadBasicInformation, &tInfo, sizeof(tInfo), &bytes);
    if (!NT_SUCCESS(status))
    {
        DPRINT("ZwQueryInformationThread failed : NTSTATUS(%#x)", status);
        goto cleanup;
    }
    status = STATUS_SUCCESS;
    threadInfo->ThreadId = cid.UniqueThread;
    threadInfo->ThreadExitCode = tInfo.ExitStatus;

cleanup:
    if (thread)
    {
        ZwClose(thread);
    }
    return status;
}

static void NTAPI KernelApcRoutine(PKAPC apc,PKNORMAL_ROUTINE* NormalRoutine,PVOID* NormalContext,PVOID* SystemArgument1,PVOID* SystemArgument2)
{
    UNREFERENCED_PARAMETER(apc);
    UNREFERENCED_PARAMETER(NormalRoutine);
    UNREFERENCED_PARAMETER(NormalContext);
    UNREFERENCED_PARAMETER(SystemArgument1);
    UNREFERENCED_PARAMETER(SystemArgument2);
}

NTSTATUS HijackThreadWithApc(HANDLE pid, void *codeAddress, void *params)
{
    PEPROCESS process = NULL;
    PETHREAD thread = NULL;
    NTSTATUS status = STATUS_SUCCESS;
    byte *procInfoBuffer = NULL;
    KAPC *apc = NULL;
    ULONG len = 0;
    ZwQuerySystemInformation(SystemProcessInformation, NULL, 0, &len);
    if (!len)
    {
        return -1;
    }
    len = (len*2 + 0x1000) & ~0xFFF;
    procInfoBuffer = ExAllocatePool(NonPagedPool, len);
    if (!procInfoBuffer)
    {
        return STATUS_INSUFFICIENT_RESOURCES;
    }
    status = ZwQuerySystemInformation(SystemProcessInformation, procInfoBuffer, len, &len);
    if (!NT_SUCCESS(status))
    {
        DPRINT("ZwQuerySystemInformation failed with NTSTATUS(%#x)", status);
        goto cleanup;
    }

    SYSTEM_PROCESS_INFORMATION *procInfo = (SYSTEM_PROCESS_INFORMATION*)procInfoBuffer;
    int success = 0;
    DPRINT("Searching for pid: %#x", pid);
    while (procInfo->NextEntryOffset)
    {
        if (procInfo->UniqueProcessId == pid)
        {
            DPRINT("Found pid: %#x, using tid: %#x", pid, procInfo->Threads[0].ClientId.UniqueThread);
            success = 1;
            break;
        }
        procInfo = (SYSTEM_PROCESS_INFORMATION*)((uintptr_t)procInfo + procInfo->NextEntryOffset);
    }
    if (!success)
    {
        DPRINT("didn't find our process!");
        status = -1;
        goto cleanup;
    }

    status = PsLookupProcessByProcessId(pid, &process);
    if (!NT_SUCCESS(status))
    {
        DPRINT("Cannot reference pid: %#x", pid);
        goto cleanup;
    }

    status = PsLookupThreadByThreadId(procInfo->Threads[0].ClientId.UniqueThread, &thread);
    if (!NT_SUCCESS(status))
    {
        DPRINT("PsLookupThreadByThreadId failed with NTSTATUS(%#x)", status);
        goto cleanup;
    }

    PKAPC_STATE apcState = (PKAPC_STATE)((uintptr_t)thread + 0x98/*ApcState offset*/);
    if (!apcState)
    {
        DPRINT("ETHREAD->ApcState = null?");
        status = -1;
        goto cleanup;
    }

    apc = (KAPC*)ExAllocatePool(NonPagedPool, sizeof(*apc));
    if (!apc)
    {
        status = STATUS_INSUFFICIENT_RESOURCES;
        goto cleanup;
    }

    KeInitializeApc(apc, thread, OriginalApcEnvironment, KernelApcRoutine, NULL, (PKNORMAL_ROUTINE)(uintptr_t)codeAddress, UserMode, params);
    DPRINT("Queueing APC");
    if (!KeInsertQueueApc(apc, 0, 0, IO_NO_INCREMENT))
    {
        status = -1;
        DPRINT("Failed to queue apc");
        goto cleanup;
    }
    BOOLEAN oState = apcState->UserApcPending;
    apcState->UserApcPending = TRUE; // force our apc to run
    Sleep(1000); // wait for thread to finish
    apcState->UserApcPending = oState; // reset 
    DPRINT("APC finished");

cleanup:
    if (procInfoBuffer) { ExFreePool(procInfoBuffer); }
    if (apc) { ExFreePool(apc); }
    if (process) { ObDereferenceObject(process); }
    if (thread) { ObDereferenceObject(thread); }
    return status;
}