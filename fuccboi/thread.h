#pragma once

#include "defs.h"

typedef struct _ThreadStartInfo
{
    LARGE_INTEGER Timeout;
    HANDLE ThreadId;
    int ThreadExitCode;
} ThreadStartInfo;

NTSTATUS ExecuteInUserThread(ThreadStartInfo *threadInfo, void *codeAddress, void *params);
NTSTATUS HijackThreadWithApc(HANDLE pid, void *codeAddress, void *params);
