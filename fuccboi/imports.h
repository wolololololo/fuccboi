#pragma once

#include "defs.h"

NTKERNELAPI NTSTATUS NTAPI PsSuspendProcess(PEPROCESS ProcessHandle);
NTKERNELAPI NTSTATUS NTAPI PsResumeProcess(PEPROCESS ProcessHandle);
NTKERNELAPI void*    NTAPI PsGetProcessPeb(PEPROCESS Process);
NTKERNELAPI NTSTATUS NTAPI ZwQuerySystemInformation(
    SYSTEM_INFORMATION_CLASS SystemInformationClass, 
    void *SystemInformation, 
    ULONG SystemInformationLength, 
    ULONG *ReturnLength);
NTKERNELAPI NTSTATUS NTAPI ZwQueryInformationThread(
    HANDLE ThreadHandle,
    THREADINFOCLASS ThreadInformationClass,
    void *ThreadInformation,
    ULONG ThreadInformationLength,
    ULONG *ReturnLength);
NTKERNELAPI NTSTATUS NTAPI ZwQueryInformationProcess(
    HANDLE ProcessHandle,
    PROCESSINFOCLASS ProcessInformationClass,
    void *ProcessInformation,
    ULONG ProcessInformationLength,
    ULONG *ReturnLength);
NTKERNELAPI NTSTATUS NTAPI RtlCreateUserThread(
    HANDLE ProcessHandle,
    SECURITY_DESCRIPTOR *SecurityDescriptor,
    BOOLEAN CreateSuspended,
    ULONG StackZeroBits,
    SIZE_T *StackReserved,
    SIZE_T *StackCommit,
    void *StartAddress,
    void *StartParameter,
    HANDLE *ThreadHandle,
    CLIENT_ID *ClientID);

NTKERNELAPI void NTAPI KeInitializeApc(
    PRKAPC Apc,
    PRKTHREAD Thread,
    KAPC_ENVIRONMENT Environment,
    PKKERNEL_ROUTINE KernelRoutine,
    PKRUNDOWN_ROUTINE RundownRoutine,
    PKNORMAL_ROUTINE NormalRoutine,
    KPROCESSOR_MODE ProcessorMode,
    void *NormalContext);
 
NTKERNELAPI BOOLEAN NTAPI KeInsertQueueApc(
    PRKAPC Apc,
    void *SystemArgument1,
    void *SystemArgument2,
    KPRIORITY Increment);
 
