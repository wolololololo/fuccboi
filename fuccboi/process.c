#include "process.h"
#include "debug.h"
#include "imports.h"

HANDLE OpenProcess(HANDLE pid)
{
    HANDLE handle;
    CLIENT_ID cid;
    RtlZeroMemory(&cid, sizeof(cid));
    OBJECT_ATTRIBUTES objAttrs;
    RtlZeroMemory(&objAttrs, sizeof(objAttrs));
    objAttrs.Length = sizeof(objAttrs);
    objAttrs.Attributes = OBJ_KERNEL_HANDLE;
    cid.UniqueProcess = pid;
    NTSTATUS status = ZwOpenProcess(&handle, PROCESS_ALL_ACCESS, &objAttrs, &cid);
    if (!NT_SUCCESS(status))
    {
        DPRINT("Failed to open process %#x : NTSTATUS(%#x)", pid, status);
        return NULL;
    }
    return handle;
}
#define PROCESS_SUSPEND_RESUME 0x800
NTSTATUS SuspendProcess(HANDLE processHandle)
{
    KPROCESSOR_MODE previousMode = ExGetPreviousMode();
    PEPROCESS process;
    NTSTATUS status = ObReferenceObjectByHandle(
        processHandle,
        PROCESS_SUSPEND_RESUME,
        *PsProcessType,
        previousMode,
        (void**)&process,
        NULL);
    if (NT_SUCCESS(status))
    {
        status = PsSuspendProcess(process);
        ObDereferenceObject(process);
    }
    return status;
}
NTSTATUS ResumeProcess(HANDLE processHandle)
{
    KPROCESSOR_MODE previousMode = ExGetPreviousMode();
    PEPROCESS process;
    NTSTATUS status = ObReferenceObjectByHandle(
        processHandle,
        PROCESS_SUSPEND_RESUME,
        *PsProcessType,
        previousMode,
        (void**)&process,
        NULL);
    if (NT_SUCCESS(status))
    {
        status = PsResumeProcess(process);
        ObDereferenceObject(process);
    }
    return status;
}
