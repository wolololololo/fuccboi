#pragma once

#include "defs.h"

NTSTATUS ReadFile(UNICODE_STRING *filename, byte **out_buffer, size_t *out_bufferSize);
BOOLEAN GetFileNameFromPath(const UNICODE_STRING *fullPath, UNICODE_STRING *outFilename);
