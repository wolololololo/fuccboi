#pragma once

#include "defs.h"

HANDLE OpenProcess(HANDLE pid);
NTSTATUS SuspendProcess(HANDLE processHandle);
NTSTATUS ResumeProcess(HANDLE processHandle);
