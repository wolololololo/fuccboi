#include "pattern.h"

static BOOLEAN MatchesPattern(const char *start, const char *pattern, const char *mask)
{
    for (; *mask; ++mask, ++start, ++pattern)
    {
        if (*mask == '?')
        {
            continue;
        }
        if (*start != *pattern)
        {
            return FALSE;
        }
    }
    return TRUE;
}
const char *FindPattern(const char *start, size_t maxDistFromStart, const char *pattern, const char *mask)
{
    for (size_t i = 0; i < maxDistFromStart; ++i)
    {
        if (MatchesPattern(&start[i], pattern, mask))
        {
            return &start[i];
        }
    }
    return NULL;
}
