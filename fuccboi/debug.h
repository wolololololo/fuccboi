#pragma once

#include <ntifs.h>

#if 1 && defined(DBG)
#define DPRINT(fmt, ...) DbgPrint("FUCC!" __FUNCTION__ " " fmt "\n", __VA_ARGS__)
#else
#define DPRINT(...) (__VA_ARGS__)
#endif
