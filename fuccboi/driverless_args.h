#pragma once

#pragma warning(disable : 4200)

typedef struct _declspec(align(8)) _Args
{
    wchar_t TargetProcess[260];
    wchar_t VulnerableDriver[260];
    wchar_t RootkitDriver[260];
    unsigned long long DllSize;
    unsigned char DllData[0];
} Args;

#pragma warning(default : 4200)
