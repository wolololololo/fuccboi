#include <intrin.h>
#include "helpers.h"

void Sleep(long long ms)
{
    LARGE_INTEGER time = { 0 };
    time.QuadPart = -ms * 10000LL;
    KeDelayExecutionThread(KernelMode, FALSE, &time);
}


/// Disables interrupts and sets with Write Protection bit(16) to 0
/// This shall have a matching SetCr0
/// @return: The original value of the cr0 register, give this value back to SetCr0
uintptr_t DisableWP(void)
{
    _disable();
    uintptr_t old = __readcr0();
    static const uintptr_t WPBit = 1 << 16;
    __writecr0(old & ~WPBit);
    return old;
}
/// Sets the cr0 register and enables interrupts
/// This is the counterpart of DisableWP
/// @param(newValue): The new value to set cr0 which sould be the result of DisableWP
void SetCr0(uintptr_t newValue)
{
    __writecr0(newValue);
    _enable();
}
