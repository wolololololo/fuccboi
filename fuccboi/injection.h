#pragma once

#include "defs.h"

typedef enum 
{
    Inject_ManualMapInUserThread,
    Inject_LoadLibraryWInUserThread,
    Inject_LdrLoadDllInUserthread
} InjectionMethod;

void InjectDllIntoPid(InjectionMethod method, wchar_t *dllPath, HANDLE pid);
NTSTATUS ManualMapImageIntoPid(byte *image, HANDLE pid);
