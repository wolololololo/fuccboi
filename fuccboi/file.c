#include "file.h"
#include "debug.h"

NTSTATUS ReadFile(UNICODE_STRING *filename, byte **out_buffer, size_t *out_bufferSize)
{
    if (!filename)
    {
        DPRINT("filename null");
        return STATUS_INVALID_PARAMETER;
    }
    if (!out_buffer)
    {
        DPRINT("out_buffer null");
        return STATUS_INVALID_PARAMETER;
    }
    if (!out_bufferSize)
    {
        DPRINT("out_bufferSize null");
        return STATUS_INVALID_PARAMETER;
    }

    OBJECT_ATTRIBUTES objAttr;
    InitializeObjectAttributes(&objAttr, filename,
        OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
        NULL, NULL);

    HANDLE handle = NULL;
    NTSTATUS status = STATUS_SUCCESS;
    IO_STATUS_BLOCK ioStatusBlock = { 0 };

    // Do not try to perform any file operations at higher IRQL levels.
    if (KeGetCurrentIrql() != PASSIVE_LEVEL)
    {
        DPRINT("Not PASSIVE_LEVEL");
        return STATUS_INVALID_DEVICE_STATE;
    }

    status = ZwCreateFile(&handle,
        GENERIC_READ,
        &objAttr, &ioStatusBlock,
        NULL,
        FILE_ATTRIBUTE_NORMAL,
        0,
        FILE_OPEN,
        FILE_SYNCHRONOUS_IO_NONALERT,
        NULL, 0);

    if (!NT_SUCCESS(status))
    {
        DPRINT("ZwCreateFile failed with NTSTATUS(%#x)", status);
        return status;
    }

    FILE_STANDARD_INFORMATION stdInfo = { 0 };
    status = ZwQueryInformationFile(handle, &ioStatusBlock, &stdInfo, sizeof(stdInfo), FileStandardInformation);
    if (!NT_SUCCESS(status))
    {
        DPRINT("ZwQueryInformationFile failed with with NTSTATUS(%#x)", status);
        goto cleanup;
    }

    ULONG length = (ULONG)stdInfo.EndOfFile.QuadPart;
    byte *buffer = ExAllocatePool(PagedPool, length);
    if (!buffer)
    {
        status = STATUS_INSUFFICIENT_RESOURCES;
        DPRINT("ExAllocatePool failed");
        goto cleanup;
    }

    LARGE_INTEGER byteOffset = { 0 };
    status = ZwReadFile(handle, NULL, NULL, NULL, &ioStatusBlock,
        buffer, length, &byteOffset, NULL);
    if (!NT_SUCCESS(status))
    {
        DPRINT("ZwReadFile failed with NTSTATUS(%#x)", status);
        ExFreePool(buffer);
        goto cleanup;
    }

    *out_buffer = buffer;
    *out_bufferSize = length;
    status = STATUS_SUCCESS;
cleanup:
    if (handle)
    {
        ZwClose(handle);
    }
    return status;
}

BOOLEAN GetFileNameFromPath(const UNICODE_STRING *fullPath, UNICODE_STRING *outFilename)
{
    if (!fullPath || !outFilename)
    {
        return FALSE;
    }
    USHORT offset = 0;
    WCHAR *filename = fullPath->Buffer;
    for (USHORT i = 0; i < fullPath->Length / sizeof(WCHAR); ++i)
    {
        if (fullPath->Buffer[i] == L'/' || fullPath->Buffer[i] == L'\\')
        {
            offset = i + 1;
            filename = &fullPath->Buffer[offset];
        }
    }
    outFilename->Buffer = filename;
    outFilename->Length = fullPath->Length - offset * sizeof(WCHAR);
    outFilename->MaximumLength = fullPath->MaximumLength - offset * sizeof(WCHAR);
    return TRUE;
}
