#pragma once

#include "defs.h"

/// Searches for a module within a user process
/// This is expected to operate within the context of said process
/// @param(process): The process
/// @param(moduleName): The name of the module
/// @return: the module base upon success, otherwise null
void *FindUserModuleBase(PEPROCESS process, wchar_t *moduleName);

/// Searches the export table of a module
/// This is expected to operate within the context of said process
/// @param(process): The process
/// @param(moduleBase): The base address of the module to search
/// @param(nameOrdinal): The name or ordinal of the exported function
/// @return: The address of the exported function or null
void *FindUserModuleExport(PEPROCESS process, void *moduleBase, const char *nameOrdinal);

/// Unlinks a module from the PEB of a user process
/// This is expected to operate within the context of said process
/// @param(process): The process
/// @param(moduleBase): The base address of the module to remove
/// @return: modulebase upon success, otherwise null
void *UnlinkUserModuleFromPeb(PEPROCESS process, void *moduleBase);

/// Searches for an unlinks a memory region from the VAD tree of
/// a process.
/// @param(process): The process
/// @param(regionBase): The memory region starting address
/// @return: regionBase upon success, otherwise null
//void *UnlinkVadFromProcess(PEPROCESS process, void *regionBase);

BOOLEAN RemoveFromUnloadedDriverList(wchar_t *entryName);