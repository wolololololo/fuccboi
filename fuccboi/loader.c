#include "loader.h"
#include "helpers.h"
#include "pattern.h"
#include "debug.h"
#include "imports.h"


void *ResolveUserForwardedExport(PEPROCESS process, char *forwardedExportAddress)
{
    // if the RVA points into the section
    // containing the export directory, this is a forwarded export. A forwarded
    // export is a pointer to an export in another binary; if it is used, the
    // pointed-to export in the other binary is used instead. The RVA in this
    // case points, as mentioned, into the export directory's section, to a
    // zero-terminated string comprising the name of the pointed-to DLL and
    // the export name separated by a dot, like "otherdll.exportname", or the
    // DLL's name and the export ordinal, like "otherdll.#19".
    wchar_t modName[512];
    char *forwardString = forwardedExportAddress;
    int i = 0;
    for (i = 0; i < 512 && forwardString[i] && forwardString[i] != '.'; ++i)
    {
        modName[i] = (wchar_t)forwardString[i];
    }
    if (forwardString[i] != '.')
    {
        DPRINT("Something went wrong with a forwarded export! (%s)", forwardString);
        return NULL;
    }
    char *nameOrd = &forwardString[i + 1]; // points after the '.'
    modName[i++] = L'.'; modName[i++] = L'd'; modName[i++] = L'l'; modName[i++] = L'l';
    modName[i] = 0;
    void *mod = FindUserModuleBase(process, modName);
    if (!mod)
    {
        DPRINT("didn't fine module %ls", modName);
        return NULL;
    }
    return FindUserModuleExport(process, mod, nameOrd);
}

void *FindUserModuleExport(PEPROCESS process, void *moduleBase, const char *nameOrdinal)
{
    IMAGE_DOS_HEADER *dosHeader = (IMAGE_DOS_HEADER*)moduleBase;
    IMAGE_NT_HEADERS64 *ntHeader = NULL;
    IMAGE_EXPORT_DIRECTORY *exportDir = NULL;
    size_t expSize = 0;
    uintptr_t address = 0;

    if (!process)
    {
        DPRINT("process null");
        return NULL;
    }

    if (!moduleBase) 
    {
        DPRINT("moduleBase null");
        return NULL;
    }

    if (dosHeader->e_magic != IMAGE_DOS_SIGNATURE) 
    {
        DPRINT("Not a PE file.");
        return NULL;
    }

    ntHeader = (IMAGE_NT_HEADERS64*)((uintptr_t)moduleBase + (dosHeader->e_lfanew));
    if (ntHeader->Signature != IMAGE_NT_SIGNATURE) 
    {
        DPRINT("Not a PE file.");
        return NULL;
    }
    // 64 bit
    if (ntHeader->OptionalHeader.Magic == IMAGE_NT_OPTIONAL_HDR64_MAGIC) 
    {
        exportDir = 
            (IMAGE_EXPORT_DIRECTORY*)(ntHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress + (uintptr_t)moduleBase);
        expSize = ntHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].Size;
    }
    // 32 bit
    else 
    {
        DPRINT("PE32 not supported!");
        return NULL;
    }

    USHORT *addressOfOrdinals = (USHORT*)(exportDir->AddressOfNameOrdinals + (uintptr_t)moduleBase);
    ULONG *addressOfNames = (ULONG*)(exportDir->AddressOfNames + (uintptr_t)moduleBase);
    ULONG *addressOfFuncs = (ULONG*)(exportDir->AddressOfFunctions + (uintptr_t)moduleBase);

    for (size_t i = 0; i < exportDir->NumberOfFunctions; ++i) 
    {
        USHORT ordinalIdx = 0xffff;
        char *name = NULL;
        
        // find by index
        if ((uintptr_t)nameOrdinal <= 0xffff) 
        {
            ordinalIdx = (USHORT)i;
        }
        // find by name
        else if ((uintptr_t)nameOrdinal > 0xffff && i < exportDir->NumberOfNames) 
        {
            name = (char*)(addressOfNames[i] + (uintptr_t)moduleBase);
            ordinalIdx = addressOfOrdinals[i];
        }
        else 
        {
            return NULL;
        }

        if (
            ((uintptr_t)nameOrdinal <= 0xffff && (USHORT)((uintptr_t)nameOrdinal) == (ordinalIdx + exportDir->Base)) ||
            ((uintptr_t)nameOrdinal > 0xffff && strcmp(name, nameOrdinal) == 0)
            ) 
        {
            address = addressOfFuncs[ordinalIdx] + (uintptr_t)moduleBase;

            if (address >= (uintptr_t)exportDir && address <= (uintptr_t)exportDir + expSize) 
            {
                return ResolveUserForwardedExport(process, (char*)address);
            }
            // found but not a forwarded export
            return (void*)address;
        }
            
    }
    return NULL;
}

void *FindUserModuleBase(PEPROCESS process, wchar_t *moduleName)
{
    if (!process) 
    {
        DPRINT("process was null.");
        return NULL;
    }
    if (!moduleName) 
    {
        DPRINT("moduleName was null.");
        return NULL;
    }
    UNICODE_STRING modName = { 0 };
    RtlInitUnicodeString(&modName, moduleName);

    PPEB peb = (PPEB)PsGetProcessPeb(process);
    if (!peb) 
    {
        DPRINT("Process has no PEB.");
        goto End;
    }

    for (int i = 0; !peb->Ldr && i < 10; ++i) 
    { // wait up to 2.5 sec
        Sleep(250);
    }
    if (!peb->Ldr) 
    {
        DPRINT("Loader didn't initialize in time.");
        goto End;
    }

    for (PLIST_ENTRY listEntry = (PLIST_ENTRY)((PPEB_LDR_DATA)peb->Ldr)->InLoadOrderModuleList.Flink;
        listEntry != &((PPEB_LDR_DATA)peb->Ldr)->InLoadOrderModuleList;
        listEntry = listEntry->Flink)
    {
        PLDR_DATA_TABLE_ENTRY entry = CONTAINING_RECORD(listEntry, LDR_DATA_TABLE_ENTRY, InLoadOrderLinks);
        if (RtlEqualUnicodeString(&entry->BaseDllName, &modName, TRUE)) 
        {
            return entry->DllBase;
        }
    }
End:
    DPRINT("User Module '%ws' not found.", moduleName);
    return NULL;
}

void *UnlinkUserModuleFromPeb(PEPROCESS process, void *moduleBase)
{
    if (!process) 
    {
        DPRINT("process was null.");
        return NULL;
    }
    if (!moduleBase) 
    {
        DPRINT("moduleBase was null.");
        return NULL;
    }

    PPEB peb = (PPEB)PsGetProcessPeb(process);
    if (!peb) 
    {
        DPRINT("Process has no PEB.");
        return NULL;
    }

    for (int i = 0; !peb->Ldr && i < 10; ++i) 
    { // wait up to 2.5 sec
        Sleep(250);
    }
    if (!peb->Ldr) 
    {
        DPRINT("Ldr didn't initialize in time.");
        return NULL;
    }
    PLIST_ENTRY head = (PLIST_ENTRY)((PPEB_LDR_DATA)peb->Ldr)->InLoadOrderModuleList.Flink;
    PLDR_DATA_TABLE_ENTRY entry = CONTAINING_RECORD(head, LDR_DATA_TABLE_ENTRY, InLoadOrderLinks);
    while (entry && entry->DllBase)
    {
        if (entry->DllBase == moduleBase)
        {
#define UNLINK(e) (e).Blink->Flink = (e).Flink; \
                  (e).Flink->Blink = (e).Blink
            UNLINK(entry->InLoadOrderLinks);
            UNLINK(entry->InInitializationOrderLinks);
            UNLINK(entry->InMemoryOrderLinks);
            //UNLINK(entry->HashLinks);

            RtlSecureZeroMemory(entry, sizeof(*entry));
            return moduleBase;
        }

        entry = (PLDR_DATA_TABLE_ENTRY)entry->InLoadOrderLinks.Flink;
    }
    return NULL;
}

//typedef struct _CONTROL_AREA
//{

//} CONTROL_AREA, *PCONTROL_AREA;

//typedef struct _MMVAD
//{

//} MMVAD, *PMMVAD;

//void *UnlinkVadFromProcess(PEPROCESS process, void *regionBase)
//{
//}

typedef struct _UNLOADED_DRIVER
{
    UNICODE_STRING EntryName;
    void *StartAddress;
    void *EndAddress;
    LARGE_INTEGER Timestamp;
} UNLOADED_DRIVER, *PUNLOADED_DRIVER;

static BOOLEAN FindUnloadedDriverList(UNLOADED_DRIVER **mmUnloadedDrivers, int **mmLastUnloadedDriver)
{
    if (!mmUnloadedDrivers)
    {
        DPRINT("mmUnloadedDrivers null");
        return FALSE;
    }
    if (!mmLastUnloadedDriver)
    {
        DPRINT("mmLastUnloadedDriver null");
        return FALSE;
    }
    UNICODE_STRING strFn = { 0 };
    RtlInitUnicodeString(&strFn, L"MmUnloadSystemImage");
    void *MmUnloadSystemImage = MmGetSystemRoutineAddress(&strFn);
    if (!MmUnloadSystemImage)
    {
        DPRINT("Didn't find %wZ", &strFn);
        return FALSE;
    }
    DPRINT("%wZ = [%p]", &strFn, MmUnloadSystemImage);
    const char *tmp = FindPattern((const char*)MmUnloadSystemImage, 0x100, "\x48\x8B\xD8\xE8\x00\x00\x00\x00", "xxxx????");
    if (!tmp)
    {
        DPRINT("Didn't find pattern for MiUnloadSystemImage");
        return FALSE;
    }
    tmp += 4;
    uintptr_t MiUnloadSystemImage = (uintptr_t)(tmp + 4 + *(int*)tmp);
    DPRINT("MiUnloadSystemImage = [%p]", MiUnloadSystemImage);

    tmp = FindPattern((const char*)MiUnloadSystemImage, 0x400, "\x48\x8D\x4B\x58\x49\xC1\xE0\x0C\x00\x8B\xD6\xE8\x00\x00\x00\x00", "xxxxxxxx?xxx????");
    if (!tmp)
    {
        DPRINT("Didn't find pattern for MiRememberUnloadedDriver");
        return FALSE;
    }
    tmp += 12;
    uintptr_t MiRememberUnloadedDriver = (uintptr_t)(tmp + 4 + *(int*)tmp);
    DPRINT("MiRememberUnloadedDriver = [%p]", MiRememberUnloadedDriver);

    const char *p = FindPattern((const char*)MiRememberUnloadedDriver, 0x100, "\xE8\x00\x00\x00\x00\x00\x8B\x00\x00\x00\x00\x00\x00\x85\x00\x0F\x84\x00\x00\x00\x00\x8B\x05\x00\x00\x00\x00\x83\x00\x32", "x?????x??????x?xx????xx????x?x");
    if (!p)
    {
        DPRINT("Didn't find pattern for MmUnloadedDrivers");
        return FALSE;
    }
    tmp = p + 8;
    uintptr_t MmUnloadedDrivers = (uintptr_t)(tmp + 4 + *(int*)tmp);
    DPRINT("MmUnloadedDrivers = [%p]", MmUnloadedDrivers);

    tmp = p + 23;
    uintptr_t MmLastUnloadedDriver = (uintptr_t)(tmp + 4 + *(int*)tmp);
    DPRINT("MmLastUnloadedDriver = [%p]", MmLastUnloadedDriver);

    *mmUnloadedDrivers = *(UNLOADED_DRIVER**)MmUnloadedDrivers;
    *mmLastUnloadedDriver = (int*)MmLastUnloadedDriver;

    return TRUE;
}

BOOLEAN RemoveFromUnloadedDriverList(wchar_t *entryName)
{
    if (!entryName)
    {
        DPRINT("entryName null");
        return FALSE;
    }

    UNLOADED_DRIVER *list = NULL;
    int *num = NULL;
    if (!FindUnloadedDriverList(&list, &num))
    {
        DPRINT("FindUnloadedDriverList failed");
        return FALSE;
    }
    DPRINT("MmUnloadedDrivers %p", (void*)list);
    DPRINT("MmLastUnloadedDriver %p", (void*)*num);
    UNICODE_STRING entry = { 0 };
    RtlInitUnicodeString(&entry, entryName);
    BOOLEAN removalSuccess = FALSE;
    DPRINT("Searching for unloaded drivers with name %wZ", &entry);
    for (int i = 0; i < *num; ++i)
    {
        DPRINT("Entry:  %wZ", list[i].EntryName);
        //DPRINT(" Start: [%p]", list[i].StartAddress);
        //DPRINT(" End:   [%p]", list[i].EndAddress);
        if (RtlEqualUnicodeString(&entry, &list[i].EntryName, TRUE))
        { // This is probably leaking memory: todo? free the entry name buffer
            DPRINT("Found entry %wZ", &entry);
            if (i + 1 >= *num)
            { // Handle when the driver to remove is the last one
                RtlSecureZeroMemory(&list[i], sizeof(UNLOADED_DRIVER));
            }
            else 
            { // Shift all entries after this one back by one, effectively overwriting ours with the next
              // this is to preserve timestamp order
                for (int j = i; j < *num - 1; ++j)
                {
                    memcpy(&list[j], &list[j + 1], sizeof(UNLOADED_DRIVER));
                }
            }
            *num = *num - 1; // we removed one, so decrement the size
            --i; // fix the iterator, otherwise removing this one would skip checking the next one
            removalSuccess = TRUE;
        }
    }
    return removalSuccess;
}
