#pragma once

#include <ntdef.h>
#include <ntifs.h>
#include <stddef.h>
#include "pe_structs.h"

#pragma pack(push, 1)

struct _KPROCESS
{
    char _pad_0x0000[0x2D0];	// offs: 0x0
    unsigned long long SecurePid;	// offs: 0x2D0
}; // size: 0x2D8
static_assert(offsetof(struct _KPROCESS, SecurePid) == 0x2D0, "_KPROCESS->SecurePid is expected to be at +2D0");
static_assert(sizeof(struct _KPROCESS) == 0x2D8, "sizeof(_KPROCESS) is expected to be 0x2D8");

struct _EPROCESS
{
    struct _KPROCESS Pcb;	// offs: 0x0
    char _pad_0x02D8[0x60];	// offs: 0x2D8
    unsigned long long VirtualSize;	// offs: 0x338
    char _pad_0x0340[0xB0];	// offs: 0x340
    unsigned long long OwnerProcessId;	// offs: 0x3F0
    PEB *Peb;	// offs: 0x3F8
    char _pad_0x0400[0x50];	// offs: 0x400
    char ImageFileName[0xF];	// offs: 0x450
    char _pad_0x045F[0x349];	// offs: 0x45F
    void *_eprocess_end;	// offs: 0x7A8
}; // size: 0x7B0
static_assert(offsetof(struct _EPROCESS, Pcb) == 0x0, "_EPROCESS->Pcb is expected to be at +0");
static_assert(offsetof(struct _EPROCESS, VirtualSize) == 0x338, "_EPROCESS->VirtualSize is expected to be at +338");
static_assert(offsetof(struct _EPROCESS, OwnerProcessId) == 0x3F0, "_EPROCESS->OwnerProcessId is expected to be at +3F0");
static_assert(offsetof(struct _EPROCESS, Peb) == 0x3F8, "_EPROCESS->Peb is expected to be at +3F8");
static_assert(offsetof(struct _EPROCESS, ImageFileName) == 0x450, "_EPROCESS->ImageFileName is expected to be at +450");
static_assert(offsetof(struct _EPROCESS, _eprocess_end) == 0x7A8, "_EPROCESS->_eprocess_end is expected to be at +7A8");
static_assert(sizeof(struct _EPROCESS) == 0x7B0, "sizeof(_EPROCESS) is expected to be 0x7B0");

#pragma pack(pop)

//typedef enum _KAPC_ENVIRONMENT
//{
//    OriginalApcEnvironment,
//    AttachedApcEnvironment,
//    CurrentApcEnvironment,
//    InsertApcEnvironment
//} KAPC_ENVIRONMENT, *PKAPC_ENVIRONMENT;

typedef VOID(NTAPI *PKNORMAL_ROUTINE)(
    PVOID NormalContext,
    PVOID SystemArgument1,
    PVOID SystemArgument2);

typedef VOID KKERNEL_ROUTINE(
    PRKAPC Apc,
    PKNORMAL_ROUTINE *NormalRoutine,
    PVOID *NormalContext,
    PVOID *SystemArgument1,
    PVOID *SystemArgument2);
typedef KKERNEL_ROUTINE(NTAPI *PKKERNEL_ROUTINE);
typedef VOID(NTAPI *PKRUNDOWN_ROUTINE)(PRKAPC Apc);

typedef struct _SYSTEM_THREAD_INFORMATION
{
    LARGE_INTEGER KernelTime;
    LARGE_INTEGER UserTime;
    LARGE_INTEGER CreateTime;
    ULONG WaitTime;
    PVOID StartAddress;
    CLIENT_ID ClientId;
    KPRIORITY Priority;
    LONG BasePriority;
    ULONG ContextSwitches;
    ULONG ThreadState;
    KWAIT_REASON WaitReason;
} SYSTEM_THREAD_INFORMATION,*PSYSTEM_THREAD_INFORMATION;

typedef struct _SYSTEM_PROCESS_INFORMATION
{
    ULONG NextEntryOffset;
    ULONG NumberOfThreads;
    LARGE_INTEGER WorkingSetPrivateSize;
    ULONG HardFaultCount;
    ULONG NumberOfThreadsHighWatermark;
    ULONGLONG CycleTime;
    LARGE_INTEGER CreateTime;
    LARGE_INTEGER UserTime;
    LARGE_INTEGER KernelTime;
    UNICODE_STRING ImageName;
    LONG BasePriority;
    PVOID UniqueProcessId;
    PVOID InheritedFromUniqueProcessId;
    ULONG HandleCount;
    ULONG SessionId;
    ULONG_PTR UniqueProcessKey;
    ULONG_PTR PeakVirtualSize;
    ULONG_PTR VirtualSize;
    ULONG PageFaultCount;
    ULONG_PTR PeakWorkingSetSize;
    ULONG_PTR WorkingSetSize;
    ULONG_PTR QuotaPeakPagedPoolUsage;
    ULONG_PTR QuotaPagedPoolUsage;
    ULONG_PTR QuotaPeakNonPagedPoolUsage;
    ULONG_PTR QuotaNonPagedPoolUsage;
    ULONG_PTR PagefileUsage;
    ULONG_PTR PeakPagefileUsage;
    ULONG_PTR PrivatePageCount;
    LARGE_INTEGER ReadOperationCount;
    LARGE_INTEGER WriteOperationCount;
    LARGE_INTEGER OtherOperationCount;
    LARGE_INTEGER ReadTransferCount;
    LARGE_INTEGER WriteTransferCount;
    LARGE_INTEGER OtherTransferCount;
    SYSTEM_THREAD_INFORMATION Threads[1];
} SYSTEM_PROCESS_INFORMATION, *PSYSTEM_PROCESS_INFORMATION;

typedef unsigned char byte;

